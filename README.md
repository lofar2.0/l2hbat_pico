
# lofar_hbat_pico
Program to control LOFAR high-band antenna tiles (HBAT) using a raspberry pi pico.  
GP15 should be connected to the HBAT communication input. 

# Building code 
## Requirements
pico-sdk 

## Instructions
- mkdir build
- cd build
- cmake ../
- make
- Connect pico while pressing BOOTSEL button
- copy test.uf2 to pico

# Controlling tile
Use jupyter scripts in scripts directory (currently using raw timing)

# Monitor tile
minicom -b 115200 -D /dev/ttyACM0

# Program structure
- hba_tx.pio:  Load delay from FIFO, thereafter continiously clock data out as they arrive in FIFO
- hba_rx.pio:  Load time-out from FIFO, thereafter continiously: wait for first falling edge, measure time between falling edges and push it to FIFO until time-out.
- test.c: 
  - Main core: 
    - Put RX FIFO data into RX buffer as they arrive from PIO
    - Put TX buffer data in TX FIFO when available
  - Second core:
    - Transmit RX buffer data over USB serial when available
    - Receive TX data from USB serial and put in TX buffer
