#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include "pico/binary_info.h"
#include "pico/multicore.h"

#include "hardware/pio.h"
#include "hba_tx.pio.h"
#include "hba_rx.pio.h"

#define TX_RAW_TIME  //This will give the raw time between edges instead of decoding it
//#define SIMULATE_HBAT //Reply on request 

const uint TX_PIN = 17;  //25=LED to test
const uint RX_PIN = 17;

#define TXtimeout 0x1000
//#define TXtimeout 0x80

//Need to change norm delay constants if time is changed!!  This can be done using TX_RAW_TIME and python scripts.
inline uint16_t NormDelay(uint16_t x){  return (TXtimeout-x+19)/20;  }

#define BUFFERSIZE 512
char buffer1[BUFFERSIZE];
uint16_t buffer2[BUFFERSIZE];
uint8_t bcnt1=0,bcnt2=0;
uint8_t bcnt3=0,bcnt4=0;
uint32_t dummy;
void core1_entry() {
while (1) {
//   printf("3");
   if (bcnt3!=bcnt4) { 
#ifdef TX_RAW_TIME
      printf("%08x\n", buffer2[bcnt4]);
#else
      if (buffer2[bcnt4]==0x100)
        printf("S", buffer2[bcnt4]);
      else if (buffer2[bcnt4]==0x200)
        printf("E\n", buffer2[bcnt4]);
      else
        printf("%02x", buffer2[bcnt4]);
#endif
      bcnt4++;bcnt4%=BUFFERSIZE;
    } 
    else {
      int x = getchar_timeout_us(0);
      if (x!=PICO_ERROR_TIMEOUT){
        if (x==0) {
//          gpio_set_dir(TX_PIN, GPIO_OUT);
//          gpio_put(TX_PIN,0);
          gpio_set_pulls(TX_PIN,false,true);
        } else {
          gpio_set_pulls(TX_PIN,true,false);
          buffer1[bcnt1]=x;
          bcnt1++;bcnt1%=BUFFERSIZE;
        }
      }
    }
}};

int main() {

//  bi_decl(bi_program_description("This is a test binary."));
//  bi_decl(bi_1pin_with_name(LED_PIN, "On-board LED"));
   stdio_init_all();
    PIO pio_rx = pio1;     
    uint offset1 = pio_add_program(pio_rx, &hba_rx_program);
    uint sm1 = pio_claim_unused_sm(pio_rx, true);
    hba_rx_program_init(pio_rx, sm1, offset1, RX_PIN);

    PIO pio_tx = pio0;     
    uint offset = pio_add_program(pio_tx, &hba_tx_program);
    uint sm = pio_claim_unused_sm(pio_tx, true);
    hba_tx_program_init(pio_tx, sm, offset, TX_PIN);


    printf("Init\n");
//  gpio_init(LED_PIN);
//  gpio_set_dir(LED_PIN, GPIO_OUT);
    gpio_set_pulls(TX_PIN,true,false);
//    gpio_set_pulls(RX_PIN,true,false);
    pio_sm_put_blocking(pio_tx, sm , 12500); //Bitrate = 150e6 / val. e.g. 15k = 10kHz
    pio_sm_put_blocking(pio_rx, sm1, TXtimeout); //Timeout
    pio_sm_put_blocking(pio_tx, sm, 0x00);
    multicore_launch_core1(core1_entry);
    printf("Starting\n");
#ifdef TX_RAW_TIME
  while (1) {
    if (pio_sm_get_rx_fifo_level(pio_rx, sm1)>0) { //First priority: Keep RX FIFO empty
      buffer2[bcnt3]=(uint16_t)(pio_sm_get_blocking(pio_rx, sm1));
      bcnt3++;bcnt3%=BUFFERSIZE;
    } else if ((pio_sm_get_tx_fifo_level(pio_tx, sm)<4) && (bcnt1!=bcnt2)) {  //Second priority: Keep TX FIFO full
       pio_sm_put_blocking(pio_tx, sm, buffer1[bcnt2]);
       bcnt2++;bcnt2%=BUFFERSIZE;
    } else if (dummy++==0) printf("\n");
    
  }
#endif //TX_RAW_TIME
  uint16_t delay;
  uint8_t previous=0,bit=0,ch=0,cnt=0; 
#ifdef  SIMULATE_HBAT
  inline void addTX(uint8_t data)
  {
    buffer1[bcnt1++]=255-data;bcnt1%=BUFFERSIZE;
  }
  inline void TX_manchester(uint8_t data){
    const uint8_t HBA_lookup[16]={0xAA,0x6A,0x9A,0x5A,0xA6,0x66,0x96,0x56,0xA9,0x69,0x99,0x59,0xA5,0x65,0x95,0x55};
    addTX(HBA_lookup[(data>>4)&0x0F]);
    addTX(HBA_lookup[data&0x0F]);
  }
  inline void GiveAnswer(uint8_t start,uint8_t cnt)
  //Store broadcast data, give back per server when asked
  { static uint8_t tiledata[32];
    uint8_t x,addr;
    addr=buffer2[start];
    if (addr==0) { //Broadcast
      if (cnt!=40) return;
      start+=6;start%=BUFFERSIZE;
      for (x=0;x<32;x++) {tiledata[x]=buffer2[start++];start%=BUFFERSIZE;}
      return;
    }
    if ((addr>=1) && (addr<=16)) { //Replay on request
      if (cnt!=6) return;
      addTX(0xff);addTX(0x0F);addTX(0xa8);
      TX_manchester(buffer2[start++]|0x80);start%=BUFFERSIZE; //Adress + 0x80
      TX_manchester(buffer2[start++]);start%=BUFFERSIZE; //Length the same
      TX_manchester(tiledata[2*addr-2]); //X data
      TX_manchester(tiledata[2*addr-1]); //Y data
      TX_manchester(0x11); //CRC1
      TX_manchester(0x22); //CRC2
    //for (x=1;x<cnt;x++){TX_manchester(buffer2[start++]);start%=BUFFERSIZE;}
      addTX(0xfd);
      return;
    }
  }
#else  
  inline void GiveAnswer(uint8_t start,uint8_t cnt) {};
#endif
  inline void TXrestart() {
        if (cnt>2) {GiveAnswer((bcnt3-cnt+1)%BUFFERSIZE,cnt-1);
                    buffer2[bcnt3++]=0x200;bcnt3%=BUFFERSIZE;
          }
        previous=0;bit=6;ch=0;cnt=0; //start new character
  }
  inline void TXchar() {
    if (bit>0) {if (cnt++==0) buffer2[bcnt3++]=0x100;
               else buffer2[bcnt3++]=ch;}
    bcnt3%=BUFFERSIZE; //store last charater
    previous=0;bit=0;ch=0; //start new character
  }
  inline void addbit0(){
    ch<<=1;
    if (++bit>=8) TXchar();
    previous=0;
  }
  inline void addbit1(){
    ch<<=1;ch++;
    if (++bit>=8) TXchar();
    previous=1;
  }
  TXrestart();
  while (1) {
    if (pio_sm_get_rx_fifo_level(pio_rx, sm1)>0) { //First priority: Keep RX FIFO empty
      delay=NormDelay( (uint16_t)(pio_sm_get_blocking(pio_rx, sm1)));
//      buffer2[bcnt3++]=delay;    bcnt3%=BUFFERSIZE; 
      if (delay==0) TXrestart(); //Timeout
      else if (delay>=6) TXrestart(); //Long delay
      else if (previous==0)
                if (delay<=2) addbit0();
                else         {addbit0();addbit1();}
           else if (delay==2) addbit1();
           else if (delay==3) addbit0();
           else              {addbit0();addbit1();} 
    } else if ((pio_sm_get_tx_fifo_level(pio_tx, sm)<4) && (bcnt1!=bcnt2)) {  //Second priority: Keep TX FIFO full
       pio_sm_put_blocking(pio_tx, sm, buffer1[bcnt2]);
       bcnt2++;bcnt2%=BUFFERSIZE;
    } else if ((dummy++==0) && (cnt==0)) printf(".");
  }

}
